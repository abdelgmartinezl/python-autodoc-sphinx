.. Big Brother documentation master file, created by
   sphinx-quickstart on Wed Jul 11 19:54:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentacion - Big Brother
===========================

.. image:: _static/lapecillo.png
    :align: center
    :alt: Lapecillo


*Big Brother* es un proyecto que busca crear ciudadanos
virtuales que pueden **comer** y **dormir**, en pro de una
mejor sociedad consumista.

Esta compuesto de la clase ``Persona``. La persona normalmente tiene:

- Nombre
- Edad

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

.. automodule:: app
    :members:
    :special-members:

Quiere aprender más?
--------------------

El ciudadano virtual esta definido en https://es.wikipedia.org/wiki/Ciudadan%C3%ADa_digital


Índices y Tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
